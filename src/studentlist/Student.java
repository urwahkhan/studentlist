package studentlist;

/**
 * This class represents students in our application
 *
 * @author Paul Bonenfant
 */
public class Student {
    private String program;
    private String name;

    public Student(String name, String program) {
        this.name = name;
        this.program = program;
    }

    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    public String getProgram() {
        return program;
    }
    
    public void setProgram(String program) {
        this.program = program;
    }
    

    
}

